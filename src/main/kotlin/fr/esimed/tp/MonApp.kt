package fr.esimed.tp

fun main(){
    val mots1 = "julie"
    val mots2 = "julien"
    val retour = levenshteinDistance(mots1, mots2)
    println("Nombre de levenshtein = $retour")
}