package fr.esimed.tp

fun levenshteinDistance(chaine1:String, chaine2:String):Int {
    val chaine1longueur = chaine1.length
    val chaine2longueur = chaine2.length

    var tableau1 = Array(chaine1longueur + 1){0}
    var tableau2 = Array(chaine1longueur + 1){it}

    for (i in 1 until chaine2longueur){
        tableau1[0] = i
        for (j in 1 until chaine2longueur){
            val match = if(chaine1[j-1] == chaine2[i-1]) 0 else 1

            val replace = tableau1[j - 1]+match
            val insert = tableau1[j] + 1
            val delete  = tableau2 [j - 1] + 1
            tableau2[j] = minOf(insert, delete, replace)
        }
        val swap = tableau1
        tableau1 = tableau2
        tableau2 = swap
    }
    return tableau1[chaine1longueur]
}